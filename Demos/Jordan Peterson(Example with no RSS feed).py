from bs4 import BeautifulSoup # Beautify our RSS!
import urllib.request # Get the website!

# Change this to the page that you want
website = "https://www.jordanbpeterson.com/blog/" # Due to javascript (ew) it will only load the latest 10 articles for jp
# Getting the site 
hdr = {'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.64 Safari/537.11',
       'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
       'Accept-Charset': 'ISO-8859-1,utf-8;q=0.7,*;q=0.3',
       'Accept-Encoding': 'none',
       'Accept-Language': 'en-US,en;q=0.8',
       'Connection': 'keep-alive'}

req = urllib.request.Request(website, headers=hdr)

Error = False

try:
    page = urllib.request.urlopen(req)
except urllib.request.HTTPError as e: # Error checking
    NewsFeed = e.read()
    Error = True

if Error == True:
    Error = True # Do nothing...
else:
    NewsFeed = page.read()

"""
Time for some soup
                   
You will have to  
edit this for the 
specific website. 
"""

soup = BeautifulSoup(NewsFeed,'lxml')

headings = []
heading = soup.find_all('h4')
for x in heading:
    headings.append(x.text.strip())

descriptions = []
description = soup.find_all('p')
for x in description:
    descriptions.append(x.text.strip())

# These should not be used normally (delete)
headings.pop()
descriptions.pop()

"""
writing latest posts
to file             
"""

f = open("Jordan Peterson.html", "w")

# loop over headings and descriptions then write to html
if Error == True:
    print(NewsFeed) # Do something with NewsFeed...
else:
    count = 0
    for x in headings:
        f.write("<h1>" + headings[count] + "</h1>") # or print(x)
        f.write("<p>" + descriptions[count] + "</p>")
        count = count + 1
    # Do something with soup...
    # Write to file

