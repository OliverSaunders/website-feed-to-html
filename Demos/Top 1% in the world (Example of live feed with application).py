from bs4 import BeautifulSoup # Beautify our RSS!
import urllib.request # Get the website!

def mean(list):
	# calculate our mean (maths)
	float_list = []
	for x in list:
		float_list.append(float(x))
	calculation = sum(float_list) / len(float_list)
	return calculation

# Change this to the page that you want
website = "https://www.worlddata.info/average-income.php" 
# Getting the site
hdr = {'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.64 Safari/537.11',
       'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
       'Accept-Charset': 'ISO-8859-1,utf-8;q=0.7,*;q=0.3',
       'Accept-Encoding': 'none',
       'Accept-Language': 'en-US,en;q=0.8',
       'Connection': 'keep-alive'}

req = urllib.request.Request(website, headers=hdr)

Error = False

try:
    page = urllib.request.urlopen(req)
except urllib.request.HTTPError as e: # Error checking
    NewsFeed = e.read()
    Error = True

if Error == True:
    Error = True # Do nothing...
else:
    NewsFeed = page.read()


"""
Time for some soup
                   
You will have to  
edit this for the 
specific website. 
"""

soup = BeautifulSoup(NewsFeed,'lxml')

moneys = []
money = soup.find_all('td')
for x in money:
    moneys.append(x.text.strip())
all_moneys = [] # Average annuall and monthly income
# loop over headings and descriptions then print and fill all_moneys
if Error == True:
    print(NewsFeed) # Do something with NewsFeed...
else:
	for x in moneys:
		print(x)
		if float(x.find("$")) >= 0: # Find our moneys 
			x = x.replace('$','') # remove our dollars so we can convert to integer
			x = x.replace(',','') # remove commas
			x = x.strip() # remove whitespace to make one number
			all_moneys.append(x)

count = 2 # Start our count the annual moneys are even and the monthly are odd
annual_moneys = [] # annual is even
monthly_moneys = [] # monthly is odd
for x in all_moneys:
	if count % 2 == 0:
		annual_moneys.append(x)
	else:
		monthly_moneys.append(x)
	count = count + 1

# Output our moneys

print(annual_moneys)
print(monthly_moneys)

print("The average annual income in the world is",int(mean(annual_moneys)),"And you are in the 1% if you earn",int((mean(annual_moneys)*100)),"or more.")
print("The average monthly income in the world is",int(mean(monthly_moneys)),"And you are in the 1% if you earn",int((mean(monthly_moneys)*100)),"or more.")
print("Please note this figures are only based on 77 countries and the currency is dollars.")


# Note this is only the https://www.worlddata.info/average-income.php and therefore only counts 77 countries.
# Note our monies are counted as "moneys" because it is easier to read